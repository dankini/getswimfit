# Pull base image
FROM python:3.9.0-slim

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directory
# Command performs mkdir and cd implicitly
WORKDIR /code

# Install dependencies
RUN pip install --upgrade pip
COPY /requirements/requirements.txt /code/
RUN pip install -r /code/requirements.txt

# Copy entire 'getswimfit' project directory to /code/
COPY . /code/