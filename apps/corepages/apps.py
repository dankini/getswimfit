# apps/corepages/apps.py

from django.apps import AppConfig


class CorePagesConfig(AppConfig):
    """Class docstring.
    """
    # name = 'corepages'
    name = 'apps.corepages'
