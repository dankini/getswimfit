# apps/corepages/views.py

from django.views.generic import TemplateView


class HomePageView(TemplateView):
    """Class docstring.
    """
    template_name = 'corepages/home.html'

class AboutPageView(TemplateView):
    """Class docstring.
    """
    template_name = 'corepages/about.html'
