# apps/corepages/test_aboutpage.py

from django.test import SimpleTestCase
from django.urls import reverse, resolve

from apps.corepages.views import AboutPageView

class AboutPageTests(SimpleTestCase):

    def setUp(self):
        url = reverse('about')
        self.response = self.client.get(url)

    def test_aboutpage_status_code(self):
        self.assertEqual(self.response.status_code, 200)

    def test_aboutpage_template(self):
        self.assertTemplateUsed(self.response, 'corepages/about.html')

    def test_aboutpage_contains_correct_html(self):
        self.assertContains(self.response, 'getswimfit - about')

    def test_aboutpage_does_not_contain_incorrect_html(self):
        self.assertNotContains(self.response, 'I should not be on this page')

    def test_aboutpage_url_resolves_aboutpageview(self):
        view = resolve('/about/')
        self.assertEqual(
            view.func.__name__,
            AboutPageView.as_view().__name__
        )
