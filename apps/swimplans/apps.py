# apps/swimplans/apps.py

from django.apps import AppConfig


class SwimplansConfig(AppConfig):
    """Class docstring.
    """
    name = 'apps.swimplans'
