"""getswimfit URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# # project/urls.py

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path
# from wagtail.admin import urls as wagtailadmin_urls
# from wagtail.documents import urls as wagtaildocs_urls
# from wagtail.core import urls as wagtail_urls

# from . import views

urlpatterns = [
    # Django Admin
    # use {% url 'admin:index' %}
    # path(settings.ADMIN_URL, admin.site.urls),
    path('admin/', admin.site.urls),

    # Wagtail Admin
    # re_path(r'^cms/', include(wagtailadmin_urls)),
    # re_path(r'^documents/', include(wagtaildocs_urls)),
    # re_path(r'^pages/', include(wagtail_urls)),

    # Core Pages - Home & About
    path('', include('apps.corepages.urls')),

    # User Management
    path('accounts/', include('allauth.urls')),

    # App URLs include statements here
    # path('newapp', include('apps.newapp.urls')),

]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        path('__debug__/', include(debug_toolbar.urls)),
    ]
    # Serve static and media files from development server
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
