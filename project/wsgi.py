"""
WSGI config for getswimfit project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

# project/wsgi.py

import os

from django.core.wsgi import get_wsgi_application

# change last arg depending on env settings
# settings. then localdev, production, staging, test
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.settings')

application = get_wsgi_application()
